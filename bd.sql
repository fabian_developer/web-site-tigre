CREATE TABLE `tbltipo_usuario` (
  `intIdTipo_usuario` int(10) NOT NULL AUTO_INCREMENT,
  `vchTipoUsuario` varchar(50) NOT NULL,
  `intNivelUsuario` int(2) NOT NULL,
  PRIMARY KEY (`intIdTipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `intIdTipo_usuario` int(10) NOT NULL,
  CONSTRAINT `tipo_usuario` FOREIGN KEY (`intIdTipo_usuario`) REFERENCES `tbltipo_usuario` (`intIdTipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblestados` (
  `intIdEstado` int(10) NOT NULL AUTO_INCREMENT,
  `vchClave` varchar(2) NOT NULL,
  `vchNombre` varchar(20) NOT NULL,
  `vchAbrev` varchar(10) NOT NULL,
  `vchActivo` int(1) NOT NULL,
  PRIMARY KEY (`intIdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblmunicipios` (
  `intIdMunicipio` int(10) NOT NULL AUTO_INCREMENT,
  `vchClave` varchar(3) NOT NULL,
  `vchNombre` varchar(100) NOT NULL,
  `vchActivo` int(1) NOT NULL,
  PRIMARY KEY (`intIdMunicipio`),
  `intIdEstado` int(10) NOT NULL,
  CONSTRAINT `estados` FOREIGN KEY (`intIdEstado`) REFERENCES `tblestados` (`intIdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tbllocalidades` (
  `intIdLocalidad` int(10) NOT NULL AUTO_INCREMENT,
  `vchClave` varchar(4) NOT NULL,
  `vchNombre` varchar(100) NOT NULL,
  `vchLatitud` varchar(20) NOT NULL,
  `vchLongitud` varchar(20) NOT NULL,
  `vchAltitud` varchar(20) NOT NULL,
  `vchActivo` int(1) NOT NULL,
  PRIMARY KEY (`intIdLocalidad`),
  `intIdMunicipio` int(10) NOT NULL,
  CONSTRAINT `municipios` FOREIGN KEY (`intIdMunicipio`) REFERENCES `tblmunicipios` (`intIdMunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tbltipo_persona` (
  `intIdTipo_persona` int(10) NOT NULL AUTO_INCREMENT,
  `vchTipoPersona` varchar(50) NOT NULL,
  `vchDescripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`intIdTipo_persona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblunidad` (
  `intIdUnidad` int(10) NOT NULL AUTO_INCREMENT,
  `vchNombre` varchar(50) NOT NULL,
  `vchComentarios` varchar(200) NOT NULL,
  PRIMARY KEY (`intIdUnidad`),
  `intIdMunicipio` int(10) NOT NULL,
  CONSTRAINT `municipios_` FOREIGN KEY (`intIdMunicipio`) REFERENCES `tblmunicipios` (`intIdMunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblpersona` (
  `intIdPersona` int(10) NOT NULL AUTO_INCREMENT,
  `vchNombre` varchar(50) NOT NULL,
  `vchApellidoPaterno` varchar(50) NOT NULL,
  `vchApellidoMaterno` varchar(50) NOT NULL,
  `vchCorreo` varchar(150) NOT NULL,
  `vchTelefono` varchar(12) NOT NULL,
  PRIMARY KEY (`intIdPersona`),
  `intIdUnidad` int(10) NOT NULL,
  `intIdTipo_persona` int(10) NOT NULL,
  CONSTRAINT `unidad` FOREIGN KEY (`intIdUnidad`) REFERENCES `tblunidad` (`intIdUnidad`),
  CONSTRAINT `Tipo_persona` FOREIGN KEY (`intIdTipo_persona`) REFERENCES `tbltipo_persona` (`intIdTipo_persona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblpaciente` (
  `intIdPaciente` int(10) NOT NULL AUTO_INCREMENT,
  `vchNum_expediente` varchar(50) NOT NULL,
  `vchCurp` varchar(20) NOT NULL,
  `vchNombre` varchar(50) NOT NULL,
  `vchApellidoPaterno` varchar(50) NOT NULL,
  `vchApellidoMaterno` varchar(50) NOT NULL,
  `intEdad` int(3) NOT NULL,
  `vchSexo` varchar(15) NOT NULL,
  `vchGam` varchar(5) NOT NULL,
  `vchMunicipio` varchar(100) NOT NULL,
  `vchLocalidad` varchar(100) NOT NULL,
  `vchDomicilio` varchar(200) NOT NULL,
  `dtFecha_registro` date NOT NULL,
  PRIMARY KEY (`intIdPaciente`),
  `intIdUnidad` int(10) NOT NULL,
  CONSTRAINT `unidad_` FOREIGN KEY (`intIdUnidad`) REFERENCES `tblunidad` (`intIdUnidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tblMuestra` (
  `intIdMuestra` int(10) NOT NULL AUTO_INCREMENT,
  `vchPersona_solicita` int(10) NOT NULL, 
  `vchFija_muestra` int(10) NOT NULL,
  `vchEspecificacion` varchar(120) NULL,
  `dtFecha_recepcion` date NOT NULL,
  `dtDiagnostico_toma` date NOT NULL,
  `vchUrgente` varchar(5) NOT NULL,
  `vchGam` varchar(5) NOT NULL,
  `vchObservacion` varchar(250) NOT NULL,
  `vchCalidad_ext` varchar(50) NOT NULL,
  `vchCalidad_muestra` varchar(50) NOT NULL,
  `intNum_laboratorio` int(10) NOT NULL,
  `intNum_muestra` int(10) NOT NULL,
  `vchResultado` varchar(50) NOT NULL,
  `dtFecha_lectura` date NOT NULL,
  `vchPersonaResponsable` varchar(120) NULL,
  `intStatus` int(2) NOT NULL,
  PRIMARY KEY (`intIdMuestra`),
  `intIdPaciente` int(10) NOT NULL,
  CONSTRAINT `paciente_` FOREIGN KEY (`intIdPaciente`) REFERENCES `tblpaciente` (`intIdPaciente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- CREATE TABLE `tblMuestra` (
--   `intIdMuestra` int(10) NOT NULL AUTO_INCREMENT,
--   `vchPersona_solicita` varchar(100) NOT NULL,
--   `vchFija_muestra` varchar(120) NOT NULL,
--   `vchEspecificacion` varchar(120) NULL,
--   `dtFecha_recepcion` date NOT NULL,
--   `dtDiagnostico_toma` date NOT NULL,
--   `vchUrgente` varchar(5) NOT NULL,
--   `vchGam` varchar(5) NOT NULL,
--   `vchObservacion` varchar(250) NOT NULL,
--   `vchCalidad_ext` varchar(50) NOT NULL,
--   `vchCalidad_muestra` varchar(50) NOT NULL,
--   `intNum_laboratorio` int(10) NOT NULL,
--   `intNum_muestra` int(10) NOT NULL,
--   `vchResultado` varchar(50) NOT NULL,
--   `dtFecha_lectura` date NOT NULL,
--   `vchPersonaResponsable` varchar(120) NULL,
--   PRIMARY KEY (`intIdMuestra`),
--   `intIdPaciente` int(10) NOT NULL,
--   CONSTRAINT `paciente_` FOREIGN KEY (`intIdPaciente`) REFERENCES `tblpaciente` (`intIdPaciente`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;