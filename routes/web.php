<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




/*
|--------------------------------------------------------------------------
| Administrador
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
*/









/*
|--------------------------------------------------------------------------
| Doctor
|--------------------------------------------------------------------------
*/

Route::get('/doctor/paciente', 'PacienteController@index_doctor_pacientes');
Route::get('/doctor/paciente.lista_pacientes', 'PacienteController@lista_pacientes');
Route::get('/doctor/paciente.detalle', 'PacienteController@paciente_detalle');
Route::post('/doctor/paciente.informacion_paciente', 'PacienteController@guardar_datos_paciente');
Route::post('/doctor/paciente.eliminar_informacion_paciente', 'PacienteController@eliminar_registro_paciente');
Route::get('/doctor/paciente.varifica_curp_paciente', 'PacienteController@varifica_curp_paciente');

/*
|--------------------------------------------------------------------------
*/








/*
|--------------------------------------------------------------------------
| Enfermera
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
*/







/*
|--------------------------------------------------------------------------
| Responsable unidad medica
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
*/