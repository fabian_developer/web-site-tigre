<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'tbllocalidades';
    public $timestamps = false;
    protected $primaryKey = 'intIdLocalidad';
    protected $fillable   = ['intIdLocalidad', 'vchClave', 'vchNombre', 'vchLatitud','vchLongitud','vchAltitud','vchActivo','intIdMunicipio'];
}
