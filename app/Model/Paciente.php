<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'tblpaciente';
    public $timestamps = false;
    protected $primaryKey = 'intIdPaciente';
    protected $fillable   = ['intIdPaciente', 'vchNum_expediente', 'vchCurp', 'vchNombre','vchApellidoPaterno','vchApellidoMaterno','intEdad','vchSexo','vchGam','vchDomicilio','dtFecha_registro','intIdUnidad'];
}

// 'vchMunicipio','vchLocalidad',