<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'tblmunicipios';
    public $timestamps = false;
    protected $primaryKey = 'intIdMunicipio';
    protected $fillable   = ['intIdMunicipio', 'intIdEstado', 'vchClave', 'vchNombre','vchNombre','vchActivo'];
}
