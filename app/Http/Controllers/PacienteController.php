<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Paciente;
use App\Model\Municipio;
use App\Model\Localidad;
use DB;
class PacienteController extends Controller
{

/*
|--------------------------------------------------------------------------
| Doctor
|--------------------------------------------------------------------------
*/

    public function index_doctor_pacientes()
    {
        // $municipio = Municipio::all();
        // $localidad = Localidad::all();
        return view('doctor.paciente.index');
        // ->with(['municipio' => $municipio])->with(['localidad' => $localidad])
    }

    public function lista_pacientes(Request $request)
    {
        $pacientes =Paciente::paginate(20);
        if ($request->ajax()) {
            return response()->json(view('doctor.paciente.lista')->with(['pacientes' => $pacientes])->render());
        }
    }

    public function paciente_detalle (Request $request)
    {
        $datos = Paciente::where('intIdPaciente', $request->clave)->first();
        echo json_encode($datos);
    }

    public function varifica_curp_paciente(Request $request)
    {
        $datos = Paciente::where('vchCurp', $request->curp)->first();
        echo json_encode($datos);
    }

    public function guardar_datos_paciente(Request $request)
    {
        $respuesta="";
        $resultado="";
        $id=$request->id;
        // $consulta=Paciente::all();
        // foreach ($consulta as $items)
        // {  
        //     if ($items['vchCurp']==$curp)
        //     {//ya existe
        //         $resultado="1";
        //     }
        // }

        if ($id=="" || $id==null)
            {//guardar
                Paciente::insert([
                    'intIdPaciente' => $id, 
                    'vchNum_expediente' => $request->num_expediente, 
                    'vchCurp' => $request->curp, 
                    'vchNombre' => $request->nombre, 
                    'vchApellidoPaterno' => $request->apellido_paterno, 
                    'vchApellidoMaterno' => $request->apellido_materno, 
                    'intEdad' => $request->edad, 
                    'vchSexo' => $request->sexo, 
                    'vchGam' => $request->gam, 
                    'vchDomicilio' => $request->direccion, 
                    'dtFecha_registro' => date("Y-n-j"), 
                    'intIdUnidad' =>$request->unidad
                ]);
                $respuesta="success";    
            }
            else
            { //modificar
                Paciente::where('intIdPaciente', $id)
                ->update([
                    'vchNum_expediente' => $request->num_expediente, 
                    'vchCurp' => $request->curp, 
                    'vchNombre' => $request->nombre, 
                    'vchApellidoPaterno' => $request->apellido_paterno, 
                    'vchApellidoMaterno' => $request->apellido_materno, 
                    'intEdad' => $request->edad, 
                    'vchSexo' => $request->sexo, 
                    'vchGam' => $request->gam, 
                    'vchDomicilio' => $request->direccion, 
                    'dtFecha_registro' => date("Y-n-j"), 
                    'intIdUnidad' =>$request->unidad
                ]);
                $respuesta="success";
            } 
        echo json_encode($respuesta);
    }

    public function eliminar_registro_paciente(Request $request)
    {
        $respuesta="";
        Paciente::where('intIdPaciente', $request->clave)->delete();
        $respuesta="success";
        echo json_encode($respuesta);
    }


/*
|--------------------------------------------------------------------------
*/


}
