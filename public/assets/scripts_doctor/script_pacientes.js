$(document).ready(function () {
    lista();

    $("#btn-nuevo_registro").click(function () {
        $('#modal-pacientes').modal('show');
        readOnly_false();
        inputs();
        $('#btn-editar').hide();
    });

    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        $.ajax({
            url: 'paciente.lista_pacientes',
            data: {
                page: page
            },
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $(".lista_pacientes").html(data);
            }
        });
    });

    $("#btn-guardar-datos").click(function () {
        var id = document.getElementById('id').value;
        var curp = document.getElementById('curp').value;
        var num_expediente = document.getElementById('num_expediente').value;
        var nombre = document.getElementById('nombre').value;
        var apellido_paterno = document.getElementById('apellido_paterno').value;
        var apellido_materno = document.getElementById('apellido_materno').value;
        var edad = document.getElementById('edad').value;
        var sexo = document.getElementById('sexo').value;
        var gam = document.getElementById('gam').value;
        var direccion = document.getElementById('direccion').value;
        var unidad = document.getElementById('unidad').value;
        var token = $("#token").val();

        $.ajax({
            method: "post",
            dataType: "json",
            url: "paciente.informacion_paciente",
            data: {
                id,
                num_expediente,
                curp,
                nombre,
                apellido_paterno,
                apellido_materno,
                edad,
                sexo,
                gam,
                direccion,
                unidad,
                _token: token
            }
        }).done(function (data) {
            if (data == "success") {
                $('#modal-pacientes').modal('hide');
                inputs();
                lista();
                swal({
                    title: "¡Bien hecho!",
                    text: "¡Información guardada con exito!",
                    icon: "success",
                    button: "Aceptar",
                });
            }

        }).fail(function () {
            //
        }).always(function () {
            //
        });
    });

    $("#btn-editar").click(function () {
        readOnly_false();

        $('#btn-guardar-datos').show();
        $('#btn-editar').hide();
    });

    validacion_input();
});

function validacion_input() {
    $("#curp").change(function () {
        var curp = document.getElementById('curp').value;
        if (curpValida(curp)) {
            alert("correcto");

            verifica_curp(curp);
            //si el formato es correcto realizara una consulta
        } else {
            alert("incorrecto");
        }
    });
}


function curpValida(curp) {
    var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0\d|1[0-2])(?:[0-2]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
        validado = curp.match(re);
    if (!validado) //Coincide con el formato general?
        return false;
    //Validar que coincida el dígito verificador
    function digitoVerificador(curp17) {
        //Fuente https://consultas.curp.gob.mx/CurpSP/
        var diccionario = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
            lngSuma = 0.0,
            lngDigito = 0.0;
        for (var i = 0; i < 17; i++) lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
        lngDigito = 10 - lngSuma % 10;
        if (lngDigito == 10) return 0;
        return lngDigito;
    }
    if (validado[2] != digitoVerificador(validado[1])) return false;
    return true; //Validado
}

function detalle(clave) {
    var token = $("#token").val();
    $.ajax({
        method: "get",
        dataType: "json",
        url: "paciente.detalle",
        data: {
            clave,
            _token: token
        }
    }).done(function (data) {
        datos(data);
        readOnly_true();
        $('#titulo_modal').html("Datos del paciente");
        $('#modal-pacientes').modal('show');
        // 
        $('#btn-editar').show();
        $('#btn-guardar-datos').hide();
    }).fail(function () {
        // 
    }).always(function () {
        // 
    });
}

function verifica_curp(curp) {
    var token = $("#token").val();
    $.ajax({
        method: "get",
        dataType: "json",
        url: "paciente.varifica_curp_paciente",
        data: {
            curp,
            _token: token
        }
    }).done(function (data) {
        if (data != null) {
            datos(data);
        }

    }).fail(function () {
        // 
    }).always(function () {
        // 
    });
}

function datos(data) {
    $('#id').val(data.intIdPaciente);
    $('#curp').val(data.vchCurp);
    $('#num_expediente').val(data.vchNum_expediente);
    $('#nombre').val(data.vchNombre);
    $('#apellido_paterno').val(data.vchApellidoPaterno);
    $('#apellido_materno').val(data.vchApellidoMaterno);
    $('#edad').val(data.intEdad);
    $('#sexo').val(data.vchSexo);
    $('#gam').val(data.vchGam);
    $('#direccion').val(data.vchDomicilio);
    $('#unidad').val(data.intIdUnidad);
}

function readOnly_true() {
    document.getElementById("curp").readOnly = true;
    document.getElementById("num_expediente").readOnly = true;
    document.getElementById("nombre").readOnly = true;
    document.getElementById("apellido_paterno").readOnly = true;
    document.getElementById('apellido_materno').readOnly = true;
    document.getElementById('edad').readOnly = true;
    document.getElementById('sexo').disabled = true;
    document.getElementById('gam').disabled = true;
    document.getElementById('direccion').readOnly = true;
    document.getElementById('unidad').readOnly = true;
}

function readOnly_false() {
    document.getElementById("curp").readOnly = false;
    document.getElementById("num_expediente").readOnly = false;
    document.getElementById("nombre").readOnly = false;
    document.getElementById("apellido_paterno").readOnly = false;
    document.getElementById('apellido_materno').readOnly = false;
    document.getElementById('edad').readOnly = false;
    document.getElementById('sexo').disabled = false;
    document.getElementById('gam').disabled = false;
    document.getElementById('direccion').readOnly = false;
    document.getElementById('unidad').readOnly = false;

}

function inputs() {
    var id = document.getElementById('id').value = "";
    var curp = document.getElementById('curp').value = "";
    var num_expediente = document.getElementById('num_expediente').value = "";
    var nombre = document.getElementById('nombre').value = "";
    var apellido_paterno = document.getElementById('apellido_paterno').value = "";
    var apellido_materno = document.getElementById('apellido_materno').value = "";
    var edad = document.getElementById('edad').value = "";
    var sexo = document.getElementById('sexo').value = "";
    var gam = document.getElementById('gam').value = "";
    var direccion = document.getElementById('direccion').value = "";
    var unidad = document.getElementById('unidad').value = "";
}



function lista() {
    $.ajax({
        url: 'paciente.lista_pacientes',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $(".lista_pacientes").html(data);
        }
    });
}

function eliminar_registro(clave) {
    var token = $("#token").val();
    swal({
        title: "¿Estás seguro?",
        text: "Una vez que se elimine, ¡no podrá recuperar este registro!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: "post",
                dataType: 'json',
                data: {
                    clave,
                    _token: token
                },
                url: "paciente.eliminar_informacion_paciente"
            }).done(function (data) {
                if (data == "success") {
                    swal({
                        title: "¡Bien hecho!",
                        text: "¡Registro eliminado con exito!",
                        icon: "success",
                        button: "Aceptar",
                    });
                    lista();
                }
            }).fail(function () {
                // 
            }).always(function () {
                //
            });
        } else {
            swal("El registro es seguro, ¡Eliminación cancelada!");
        }
    });
}
