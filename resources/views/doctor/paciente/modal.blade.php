<div class="modal fade bd-example-modal-lg" id="modal-pacientes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <form>
                            <input class="form-control" id="token" name="token" readonly="" type="hidden" value="{{ csrf_token() }}" />
                            <input class="form-control" id="id" name="id" type="hidden"/>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Curp</label>
                                        <input type="text" class="form-control" id="curp" placeholder="Ejemplo: SASO750909HDFNNS05">
                                        <small id="curpHelp" class="form-text text-muted"></small>
                                        {{-- oninput="validarInput(this)" --}}
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Número de expediente</label>
                                        <input type="text" class="form-control" id="num_expediente" placeholder="Número de expediente">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Apellido paterno</label>
                                        <input type="text" class="form-control" id="apellido_paterno" placeholder="Apellido paterno">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'lsl never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Apellido materno</label>
                                        <input type="text" class="form-control" id="apellido_materno" placeholder="Apellido materno">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Edad</label>
                                        <input type="number" class="form-control" id="edad" placeholder="Edad">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Sexo</label>
                                        <select class="form-control" id="sexo">
                                            <option value="Masculino">Masculino</option>
                                            <option value="Femenino">Femenino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Gam</label>
                                        <select class="form-control" id="gam">
                                            <option value="Si">Si</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <textarea type="text" id="direccion" class="form-control" cols="30" rows="10"></textarea>
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">

                                    <div class="form-group">
                                        <label>Unidad medica</label>
                                        <input type="email" class="form-control" id="unidad" placeholder="Unidad medica">
                                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with anyone else.</small> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="btn-guardar-datos">Guardar</button>
                                <button type="button" class="btn btn-primary" id="btn-editar">Editar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
