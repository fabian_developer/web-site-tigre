<div class="pacientes">
    @foreach ($pacientes as $items)
    <div class="card d-flex flex-row mb-3">
        <div class="d-flex flex-grow-1 min-width-zero">
            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                <a class="list-item-heading mb-1 truncate w-40 w-xs-80">
                    {{ $items->vchNombre }} {{ $items->vchApellidoPaterno }} {{ $items->vchApellidoMaterno }}
                </a>
                <a class="list-item-heading mb-1 truncate w-40 w-xs-80">
                    {{ $items->vchCurp }}
                </a>
                <a class="list-item-heading mb-1 truncate w-40 w-xs-80">
                    Núm exp.: {{ $items->vchNum_expediente }}
                </a>
                {{-- <a class="list-item-heading mb-1 truncate w-40 w-xs-80">
                    Edad: {{ $items->intEdad }}
                </a> --}}
                <a class="list-item-heading mb-1 truncate w-40 w-xs-80">
                    Gam: {{ $items->vchGam }}
                </a>
            </div>
            <div class="custom-control custom-checkbox pl-1 align-self-center pr-4">
                <button type="button" class="btn btn-success mb-1" onclick="detalle({{ $items->intIdPaciente }});">Ver</button>
                <button type="button" class="btn btn-danger mb-1" onclick="eliminar_registro({{ $items->intIdPaciente }});">Eliminar</button>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="pagination justify-content-center mb-0">
    <nav class="page-item">
        {{ $pacientes->render() }}
    </nav>
</div>